INSERT INTO unit_of_measure (code, description) values ('KG', 'kilo');
INSERT INTO unit_of_measure (code, description) values ('GR', 'gramm');

INSERT INTO permission (name) values ('delete');
INSERT INTO permission (name) values ('update');
INSERT INTO permission (name) values ('create');
