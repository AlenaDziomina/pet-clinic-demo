create table APP_USER
(
    ID INTEGER not null
        primary key,
    LOGIN VARCHAR(255),
    ROLE VARCHAR(255)
);

create table DENTIST
(
    ID BIGINT not null
        primary key,
    CREATED_AT TIMESTAMP,
    MODIFIED_AT TIMESTAMP,
    FIRST_NAME VARCHAR(255),
    LAST_NAME VARCHAR(255),
    PHONE VARCHAR(255),
    NAME0 VARCHAR(255),
    NAME1 VARCHAR(255)
);

create table NOTES
(
    ID BIGINT auto_increment
        primary key,
    DESCRIPTION VARCHAR(255)
);

create table OWNER
(
    ID BIGINT not null
        primary key,
    CREATED_AT TIMESTAMP,
    MODIFIED_AT TIMESTAMP,
    FIRST_NAME VARCHAR(255),
    LAST_NAME VARCHAR(255),
    PHONE VARCHAR(255),
    FULL_NAME VARCHAR(255)
);

create table PERMISSION
(
    ID INTEGER auto_increment
        primary key,
    NAME VARCHAR(255)
);

create table PET
(
    PET_TYPE VARCHAR(31) not null,
    ID BIGINT not null
        primary key,
    CREATED_AT TIMESTAMP,
    MODIFIED_AT TIMESTAMP,
    BIRTH_DATE DATE,
    PET_NAME VARCHAR(255) not null
        constraint UK_GFCBHSQX3C7L5TRN8I1IEJUBM
            unique,
    CAT_NAME VARCHAR(255),
    DOG_NAME VARCHAR(255)
);

create table PRODUCT
(
    CODE VARCHAR(255) not null,
    CODE_PART VARCHAR(255) not null,
    NAME VARCHAR(255),
    PRICE DECIMAL(19,2),
    primary key (CODE, CODE_PART)
);

create table RECEIPT
(
    ID BIGINT not null
        primary key,
    AMOUNT DECIMAL(19,2),
    DISCOUNT INTEGER not null
);

create table SURGERION
(
    ID BIGINT not null
        primary key,
    CREATED_AT TIMESTAMP,
    MODIFIED_AT TIMESTAMP,
    FIRST_NAME VARCHAR(255),
    LAST_NAME VARCHAR(255),
    PHONE VARCHAR(255),
    NAME0 VARCHAR(255),
    NAME2 VARCHAR(255)
);

create table UNIT_OF_MEASURE
(
    CODE VARCHAR(255) not null
        primary key,
    DESCRIPTION VARCHAR(255)
);

create table ITEM
(
    ID BIGINT not null
        primary key,
    AMOUNT DECIMAL(19,2),
    NAME VARCHAR(255),
    RECEIPT_ID BIGINT,
    UOF_CODE VARCHAR(255),
    constraint FK76GVW2RFFUJ666UC8D1X1X30D
        foreign key (UOF_CODE) references UNIT_OF_MEASURE (CODE),
    constraint FK8S305NSNV4WI5IUH7KKR52GQT
        foreign key (RECEIPT_ID) references RECEIPT (ID)
);

create table USER
(
    ID BIGINT not null
        primary key,
    CREATED_AT TIMESTAMP,
    MODIFIED_AT TIMESTAMP,
    USER_NAME VARCHAR(255),
    LAST_NAME VARCHAR(255),
    PHONE_NUMBER VARCHAR(255),
    LOGIN VARCHAR(255),
    PASSWORD VARCHAR(255),
    ROLE INTEGER
);

create table USER_PERMISSIONS
(
    USR_ID INTEGER not null,
    PERM_ID INTEGER not null,
    primary key (USR_ID, PERM_ID),
    constraint FK891Q20LH249PM79KWDNJCD0DY
        foreign key (PERM_ID) references PERMISSION (ID),
    constraint FKOE4V9RD7JFMFE9UV7RVRLJIYF
        foreign key (USR_ID) references APP_USER (ID)
);

create table VET
(
    ID BIGINT not null
        primary key,
    CREATED_AT TIMESTAMP,
    MODIFIED_AT TIMESTAMP,
    FIRST_NAME VARCHAR(255),
    LAST_NAME VARCHAR(255),
    PHONE VARCHAR(255),
    NAME0 VARCHAR(255)
);

create table VIP_CLIENT
(
    VIP_CARD VARCHAR(255),
    OWNER_ID BIGINT not null
        primary key,
    constraint FKM0V00N6XRDBW5BNHL7P79IADU
        foreign key (OWNER_ID) references OWNER (ID)
);

create table VISIT
(
    ID BIGINT auto_increment
        primary key,
    TIME TIMESTAMP,
    NOTES_ID BIGINT,
    constraint FKDTWDS0QQ9ID8T981CPM6UV3EU
        foreign key (NOTES_ID) references NOTES (ID)
);

