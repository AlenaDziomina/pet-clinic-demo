package com.learning.petclinic.example;

import lombok.extern.slf4j.Slf4j;

//@ThreadSafe
@Slf4j
public class SingletonExample {

    private SingletonExample(String path) {
        //to avoid external call
    }

    private static class LazyHolder {
        static final SingletonExample INSTANCE = new SingletonExample("aaa");
    }

    public static SingletonExample getInstance() {
        return LazyHolder.INSTANCE;
    }

    public void doSmth() {
        //
    }
}
