package com.learning.petclinic.example;

public enum SingletonEnum {

    INSTANCE("path1");

    private String path;

    SingletonEnum(String path) {
        this.path = path;
    }

    public void doSmth() {
        //
    }
}
