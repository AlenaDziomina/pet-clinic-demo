package com.learning.petclinic.example;

public class Foo {

    public static void main(String[] args) {
        SingletonExample example = SingletonExample.getInstance();
        example.doSmth();

        SingletonEnum singletonEnum = SingletonEnum.INSTANCE;
        singletonEnum.doSmth();
    }
}
