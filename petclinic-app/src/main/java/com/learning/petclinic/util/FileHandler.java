package com.learning.petclinic.util;

import com.learning.petclinic.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PreDestroy;

public class FileHandler {

    @Autowired
    private OwnerService ownerService;

    private String filePath;

    public FileHandler(String filePath, String encoding) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return ownerService == null ? "not a bean" : "is a bean" + filePath;
    }

    @PreDestroy
    public void init() {
//        file.close();
//        file.delete();
    }
}
