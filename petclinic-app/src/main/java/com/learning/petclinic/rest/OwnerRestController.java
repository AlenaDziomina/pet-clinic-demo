package com.learning.petclinic.rest;

import com.learning.petclinic.dto.CreateOwnerDto;
import com.learning.petclinic.dto.OwnerDto;
import com.learning.petclinic.service.OwnerRestService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RequestMapping("/api/owners")
@RestController
public class OwnerRestController {

    @Autowired
    private OwnerRestService ownerService;

    @ApiOperation(value = "Get owner list", response = List.class)
    //localhost:8080/api/owners - url and path
    @GetMapping
    Set<OwnerDto> getOwners() {
        return ownerService.findAll();
    }

    @ApiOperation(value = "Get owner by id", response = OwnerDto.class)
    //localhost:8080/api/owners/123 - uri and path
    @GetMapping("/{id}")
    OwnerDto getOwner(@PathVariable Long id) {
        return ownerService.findById(id);
    }

    //localhost:8080/api/owners?name=alena - path
    //localhost:8080/api/owners - url
    @GetMapping("/find")
    List<OwnerDto> findOwners(@ApiParam(value = "Owner full name") @RequestParam String name) {
        return ownerService.findByFullName(name);
    }

    //localhost:8080/api/owners - url and path
    @PostMapping
    OwnerDto createOwner(@Valid @RequestBody CreateOwnerDto owner) {
        return ownerService.save(owner);
    }

    @DeleteMapping("/{id}")
    void deleteOwner(@PathVariable Long id) {
        ownerService.deleteById(id);
    }

    @PutMapping("/{id}")
    void updateOwner(@Valid @RequestBody OwnerDto owner, @PathVariable Long id) {
        ownerService.updateOwner(id, owner);
    }

    @PutMapping("/{id}/changeStatus")
    void changeStatus(@PathVariable Long id, @RequestParam boolean newState) {
        OwnerDto existing = ownerService.findById(id);
        if (existing != null) {
            //existing.setState(newState);
        }
    }

}
