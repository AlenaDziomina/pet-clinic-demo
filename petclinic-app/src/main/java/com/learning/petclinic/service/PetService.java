package com.learning.petclinic.service;

import com.learning.petclinic.model.Owner;
import com.learning.petclinic.model.Pet;

public interface PetService extends CrudService<Pet, Long> {


    default void updatePetAndOwner(Owner owner, Pet pet) {
        throw new UnsupportedOperationException();
    }
}
