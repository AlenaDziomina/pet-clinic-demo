package com.learning.petclinic.service.map;

import com.learning.petclinic.model.Vet;
import com.learning.petclinic.service.VetService;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.HashMap;
import java.util.Map;

public class VetMapService extends AbstractMapService<Vet> implements VetService {

    private static final Map<Long, Vet> map = new HashMap<>();

    @PostConstruct
    public void init() {
//        Vet vet = Vet.builder()
//                .id(1L)
//                .firstName("Alex")
//                .lastName("Grouk")
//                .build();
//        map.put(1L, vet);
    }

    @Override
    public Map<Long, Vet> getMap() {
        return map;
    }

    @PreDestroy
    public void destroy() {
        map.clear();
    }
}
