package com.learning.petclinic.service.jpa;

import com.learning.petclinic.model.Owner;
import com.learning.petclinic.model.Pet;
import com.learning.petclinic.repository.PetRepository;
import com.learning.petclinic.service.OwnerService;
import com.learning.petclinic.service.PetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Transactional(readOnly = true)
@Service
public class PetJpaService extends AbstractJpaService<Pet, Long> implements PetService {

    @Autowired
    private PetRepository petRepository;

    @Autowired
    private OwnerService ownerService;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public JpaRepository<Pet, Long> getRepository() {
        return petRepository;
    }

    @Transactional
    @Override
    public void updatePetAndOwner(Owner owner, Pet pet) {
        Pet petFromDB = petRepository.findById(pet.getId()).orElseThrow();
        petFromDB.setName(pet.getName());

        ownerService.updateOwner(owner);
    }

    public void doSth() {
        Query query =entityManager.createNamedQuery("Pet.findAll");
        List result = query.getResultList();
    }

    @Transactional
    public Pet insertWithTr(Pet pet) {
        entityManager.persist(pet);
        return pet;
    }

}
