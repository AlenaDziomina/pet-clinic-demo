package com.learning.petclinic.service.jpa;

import com.learning.petclinic.model.Vet;
import com.learning.petclinic.repository.VetRepository;
import com.learning.petclinic.service.VetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
@Service
public class VetJpaService extends AbstractJpaService<Vet, Long> implements VetService {

    @Autowired
    private VetRepository vetRepository;

    @Override
    public JpaRepository<Vet, Long> getRepository() {
        return vetRepository;
    }
}
