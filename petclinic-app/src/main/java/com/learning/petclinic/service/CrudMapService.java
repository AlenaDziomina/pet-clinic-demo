package com.learning.petclinic.service;

import java.util.Map;

public interface CrudMapService<T, ID> extends CrudService<T, ID> {

    Map<ID, T> getMap();
}
