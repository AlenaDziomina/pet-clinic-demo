package com.learning.petclinic.service.map;

import com.learning.petclinic.model.Pet;
import com.learning.petclinic.model.PetType;
import com.learning.petclinic.service.PetService;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class PetMapService extends AbstractMapService<Pet> implements PetService {

    private static final Map<Long, Pet> map = new HashMap<>();

    @PostConstruct
    public void init() {
        Pet pet = Pet.builder()
                .id(1l)
                .petType(PetType.DOG)
                .birthDate(LocalDate.now().minusYears(1))
                .build();
        map.put(1L, pet);
    }

    @Override
    public Map<Long, Pet> getMap() {
        return map;
    }

    @PreDestroy
    public void destroy() {
        map.clear();
    }
}
