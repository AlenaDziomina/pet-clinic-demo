package com.learning.petclinic.service.map;

import com.learning.petclinic.model.Owner;
import com.learning.petclinic.service.OwnerService;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class OwnerMapService extends AbstractMapService<Owner> implements OwnerService {

    private static final Map<Long, Owner> map = new HashMap<>();

    @PostConstruct
    public void init() {
//        Owner owner = Owner.builder()
//                .id(1L)
//                .firstName("Alena")
//                .lastName("Dziomina")
//                .fullName("Alena Dziomina")
//                .build();
//        map.put(1L, owner);
    }

    @Override
    public Map<Long, Owner> getMap() {
        return map;
    }

    @Override
    public Optional<Owner> findByLastName(String lastName) {
        return getMap().values().stream().filter(o -> o.getContactDetails().getLastName().equals(lastName)).findFirst();
    }

    @PreDestroy
    public void destroy() {
        map.clear();
    }
}
