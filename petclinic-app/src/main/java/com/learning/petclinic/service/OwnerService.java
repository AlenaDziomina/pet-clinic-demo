package com.learning.petclinic.service;

import com.learning.petclinic.model.Owner;

import java.util.List;
import java.util.Optional;

public interface OwnerService extends CrudService<Owner, Long> {

    Optional<Owner> findByLastName(String lastName);

    default void updateOwner(Owner owner) {
        throw new UnsupportedOperationException();
    }

    default List<Owner> findByFullName(String fullName) {
        throw new UnsupportedOperationException();
    }
}
