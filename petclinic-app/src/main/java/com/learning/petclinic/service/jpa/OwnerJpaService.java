package com.learning.petclinic.service.jpa;

import com.learning.petclinic.model.Owner;
import com.learning.petclinic.repository.OwnerRepository;
import com.learning.petclinic.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Primary
@Transactional(readOnly = true)
@Service
public class OwnerJpaService extends AbstractJpaService<Owner, Long> implements OwnerService {

    @Autowired
    private OwnerRepository ownerRepository;

    @Override
    public Optional<Owner> findByLastName(String lastName) {
        return ownerRepository.findByContactDetailsLastName(lastName);
    }

    @Override
    public JpaRepository<Owner, Long> getRepository() {
        return ownerRepository;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRES_NEW)
    public void updateOwner(Owner owner) {
        Owner ownerFromDB = ownerRepository.findById(owner.getId()).orElseThrow();
        ownerFromDB.getContactDetails().setFirstName(owner.getContactDetails().getFirstName());
        ownerFromDB.getContactDetails().setLastName(owner.getContactDetails().getLastName());
    }

    @Override
    public List<Owner> findByFullName(String fullName) {
        return ownerRepository.findByFullNameContainingIgnoreCase(fullName);
    }

    @Transactional
    public void clean() {
        ownerRepository.deleteInactive(new Date());
    }
}
