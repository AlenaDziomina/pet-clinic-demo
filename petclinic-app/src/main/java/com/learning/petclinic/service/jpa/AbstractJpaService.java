package com.learning.petclinic.service.jpa;

import com.learning.petclinic.exception.NotFoundException;
import com.learning.petclinic.model.BaseEntity;
import com.learning.petclinic.service.CrudJpaService;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public abstract class AbstractJpaService<T extends BaseEntity, ID> implements CrudJpaService<T, ID> {
    @Override
    public T findById(ID id) {
        Optional<T> object = getRepository().findById(id);
        if (object.isEmpty()) {
            throw new NotFoundException("Entity with id " + id + " not found. Please refresh page");
        }
        return object.get();
    }

    @Override
    public T save(T object) {
        return getRepository().save(object);
    }

    @Override
    public Set<T> findAll() {
        return new HashSet<>(getRepository().findAll());
    }

    @Override
    public void delete(T object) {
        getRepository().delete(object);
    }

    @Override
    public void deleteById(ID id) {
        getRepository().deleteById(id);
    }
}
