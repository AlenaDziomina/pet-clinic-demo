package com.learning.petclinic.service;

import com.learning.petclinic.dto.CreateOwnerDto;
import com.learning.petclinic.dto.OwnerDto;
import com.learning.petclinic.exception.NotFoundException;
import com.learning.petclinic.mapper.OwnerMapper;
import com.learning.petclinic.model.ContactDetails;
import com.learning.petclinic.model.Owner;
import com.learning.petclinic.repository.OwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class OwnerRestService {

    @Autowired
    private OwnerMapper ownerMapper;
    @Autowired
    private OwnerRepository ownerRepository;


    public Set<OwnerDto> findAll() {
        return ownerRepository
                .findAll()
                .stream()
                .map(o -> ownerMapper
                        .mapToDto(o))
                .collect(Collectors.toSet());
    }

    public OwnerDto findById(Long id) {
        return ownerMapper.mapToDto(ownerRepository.findById(id).orElseThrow(NotFoundException::new));
    }

    public List<OwnerDto> findByFullName(String name) {
        return  ownerRepository
                .findByFullNameContainingIgnoreCase(name)
                .stream()
                .map(o-> ownerMapper
                        .mapToDto(o))
                .collect(Collectors.toList());
    }

    public void deleteById(Long id) {
        ownerRepository.deleteById(id);
    }

    public OwnerDto save(CreateOwnerDto ownerDto) {
        Owner owner = Owner.builder()
                .contactDetails(ContactDetails.builder()
                        .firstName(ownerDto.getFirstName())
                        .lastName(ownerDto.getLastName())
                        .build())
                .address(ownerDto.getAddress())
                .build();
        return ownerMapper.mapToDto(ownerRepository.save(owner));
    }

    @Transactional
    public void updateOwner(Long id, OwnerDto ownerDto) {
        Owner owner = ownerRepository.findById(id).orElseThrow(() -> new NotFoundException());
        ContactDetails cd = owner.getContactDetails();
        cd.setFirstName(ownerDto.getFirstName());
        cd.setLastName(ownerDto.getLastName());
        owner.setAddress(ownerDto.getAddress());

    }
}
