package com.learning.petclinic.service;

import com.learning.petclinic.model.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CrudJpaService<T extends BaseEntity, ID> extends CrudService<T, ID> {

    JpaRepository<T, ID> getRepository();
}
