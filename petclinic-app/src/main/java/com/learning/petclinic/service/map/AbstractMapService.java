package com.learning.petclinic.service.map;

import com.learning.petclinic.model.BaseEntity;
import com.learning.petclinic.service.CrudMapService;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class AbstractMapService<T extends BaseEntity> implements CrudMapService<T, Long> {

    @Override
    public T findById(Long id) {
        Map<Long, T> map = getMap();
        return map.get(id);
    }

    @Override
    public T save(T object) {
        getMap().put(object.getId(), object);
        return object;
    }

    @Override
    public Set<T> findAll() {
        Map<Long, T> map = getMap();
        return new HashSet<>(map.values());
    }

    @Override
    public void delete(T object) {
        getMap().remove(object.getId());
    }

    @Override
    public void deleteById(Long id) {
        getMap().remove(id);
    }
}
