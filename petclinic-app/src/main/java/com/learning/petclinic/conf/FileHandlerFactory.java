package com.learning.petclinic.conf;

import com.learning.petclinic.util.FileHandler;

//@FunctionalInterface
public interface FileHandlerFactory {

    FileHandler fileHandler(String path, String encoding);
}
