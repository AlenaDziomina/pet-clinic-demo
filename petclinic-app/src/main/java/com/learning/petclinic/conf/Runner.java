package com.learning.petclinic.conf;

import com.learning.petclinic.controller.OwnerController;
import com.learning.petclinic.controller.VetController;
import org.jasypt.encryption.StringEncryptor;
//import org.learning.utils.NameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Runner implements CommandLineRunner {

    @Qualifier("customEncryptor")
    @Autowired
    private StringEncryptor stringEncryptor;

    @Value("${datasource.password}")
    private String datasourcePwd;

 /*   @Autowired
    private NameUtils nameUtils;*/

    @Autowired
    private OwnerController ownerController;
    @Autowired
    private VetController vetController;

    @Override
    public void run(String... args) throws Exception {
//        String pwd = "myPwd";
//        String encryptedPwd = stringEncryptor.encrypt(pwd);
//        System.out.println("Encrypted: " + encryptedPwd);
//
//        String decrypted = stringEncryptor.decrypt(encryptedPwd);
//        System.out.println("Decrypted: " + decrypted);
//
//        System.out.println(datasourcePwd);
//
//        System.out.println(nameUtils.getFullName("Alena", "Dziomina"));
//
//        System.out.println(ownerController.encrypt("1234"));
//        System.out.println(vetController.encrypt("5678"));
//
//        System.out.println(ownerController.getFile3("aaa", "111"));
//        System.out.println(ownerController.getFile3("bbb", "222"));

    }
}
