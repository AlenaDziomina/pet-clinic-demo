package com.learning.petclinic.conf;

import com.learning.petclinic.util.FileHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.function.Function;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

@EnableTransactionManagement
@Configuration
public class AppConfig {

    @Bean
    public FileHandlerFactory fileHandlerFactory() {
        return this::fileHandler;
    }

    @Scope(SCOPE_PROTOTYPE)
    @Bean
    FileHandler fileHandler(String filePath, String encoding) {
        return new FileHandler(filePath, encoding);
    }


}
