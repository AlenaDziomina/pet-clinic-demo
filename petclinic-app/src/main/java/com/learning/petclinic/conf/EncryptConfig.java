package com.learning.petclinic.conf;

import com.ulisesbocchio.jasyptspringboot.encryptor.SimplePBEByteEncryptor;
import com.ulisesbocchio.jasyptspringboot.encryptor.SimplePBEStringEncryptor;
import org.jasypt.encryption.StringEncryptor;
import org.jasypt.encryption.pbe.PBEByteEncryptor;
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.jasypt.salt.SaltGenerator;
import org.jasypt.salt.StringFixedSaltGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class EncryptConfig {

    @Value("${jasypt.encryptor.algorithm:PBEWithMD5AndDES}")
    public String ALGORITHM;
    @Value("${jasypt.encryptor.password:password}")
    public String PASSWORD;

    @Bean("polledEncryptor")
    public StringEncryptor stringEncryptor(SaltGenerator saltGenerator) {
        PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
        SimpleStringPBEConfig config = new SimpleStringPBEConfig();
        config.setPassword(PASSWORD);
        config.setAlgorithm(ALGORITHM);
        config.setPoolSize(5);
        config.setSaltGenerator(saltGenerator);
        config.setKeyObtentionIterations(1000);

        encryptor.setConfig(config);
        return encryptor;
    }

    @Scope(value = "prototype")
    @Bean("customEncryptor")
    public StringEncryptor stringEncryptor(PBEByteEncryptor delegate) {
        return new SimplePBEStringEncryptor(delegate);
    }

    @Bean
    public PBEByteEncryptor byteEncryptor(SaltGenerator saltGenerator) {
        SimplePBEByteEncryptor byteEncryptor = new SimplePBEByteEncryptor();
        byteEncryptor.setPassword(PASSWORD);
        byteEncryptor.setAlgorithm(ALGORITHM);
        byteEncryptor.setSaltGenerator(saltGenerator);
        byteEncryptor.setIterations(2);
        return byteEncryptor;
    }

    @Bean
    public SaltGenerator saltGenerator() {
        return new StringFixedSaltGenerator("some_salt");
    }


}
