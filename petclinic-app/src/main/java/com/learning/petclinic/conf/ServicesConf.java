package com.learning.petclinic.conf;

import com.learning.petclinic.service.OwnerService;
import com.learning.petclinic.service.PetService;
import com.learning.petclinic.service.VetService;
import com.learning.petclinic.service.map.OwnerMapService;
import com.learning.petclinic.service.map.PetMapService;
import com.learning.petclinic.service.map.VetMapService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;


@Profile("dev")
@Configuration
public class ServicesConf {

    @Bean
    public OwnerService ownerService() {
        return new OwnerMapService();
    }

    @Bean
    public VetService vetService() {
        return new VetMapService();
    }

    @Bean
    public PetService petService() {
        return new PetMapService();
    }

}
