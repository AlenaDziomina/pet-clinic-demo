package com.learning.petclinic.controller;

import com.learning.petclinic.model.Owner;
import com.learning.petclinic.service.OwnerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class IndexController {

    private final OwnerService ownerService;

    public IndexController(OwnerService ownerService) {
        this.ownerService = ownerService;
    }


    @RequestMapping(value = {"", "/", "index", "index.html"})
    public String index(Model model) {
        System.out.println("test");
        Set<Owner> ownerSet = ownerService.findAll();
        model.addAttribute("owners", ownerSet);
        return "index";
    }
}
