package com.learning.petclinic.controller;

import com.learning.petclinic.service.VetService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@RequiredArgsConstructor
@Controller
public class VetController {

    private final VetService vetService;

    @GetMapping("/vets")
    public String getVets(Model model) {
        model.addAttribute("vets", vetService.findAll());
        return "vets/index";
    }

}
