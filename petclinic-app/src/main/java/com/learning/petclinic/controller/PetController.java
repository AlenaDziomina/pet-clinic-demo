package com.learning.petclinic.controller;

import com.learning.petclinic.service.PetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PetController {

    @Autowired
    private PetService petService;

    @GetMapping("/pets")
    public String getPets(Model model) {
        model.addAttribute("pets", petService.findAll());
        return "pets/index";
    }


}
