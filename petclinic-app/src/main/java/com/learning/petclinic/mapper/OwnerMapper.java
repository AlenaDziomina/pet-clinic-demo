package com.learning.petclinic.mapper;

import com.learning.petclinic.dto.OwnerDto;
import com.learning.petclinic.model.Owner;
import org.springframework.stereotype.Component;

@Component
public class OwnerMapper {

    public OwnerDto mapToDto(Owner owner) {
        OwnerDto dto = new OwnerDto();
        dto.setId(owner.getId());
        dto.setAddress(owner.getAddress());
        dto.setFirstName(owner.getContactDetails().getFirstName());
        dto.setLastName(owner.getContactDetails().getLastName());
        dto.setFillName(owner.getFullName());
        return dto;
    }
}
