package com.learning.petclinic.service.jpa;

import com.learning.petclinic.model.ContactDetails;
import com.learning.petclinic.model.Owner;
import com.learning.petclinic.repository.OwnerRepository;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

//import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

//@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class OwnerJpaServiceTest {

    @InjectMocks
    OwnerJpaService ownerJpaService;

    @Mock
    OwnerRepository ownerRepository;


    @Test
    public void findByLastName() {
        //given
        String lastName = "LastName";
        Owner owner = Owner.builder()
                .contactDetails(ContactDetails.builder()
                        .lastName(lastName)
                        .build())
                .build();
        when(ownerRepository.findByContactDetailsLastName(lastName)).thenReturn(Optional.of(owner));


        //when
        Optional<Owner> result = ownerJpaService.findByLastName(lastName);

        //then
        assertTrue(result.isPresent());
        assertEquals(lastName, result.get().getContactDetails().getLastName());
    }
}