package com.learning.petclinic.repository;

import com.learning.petclinic.model.ContactDetails;
import com.learning.petclinic.model.Dog;
import com.learning.petclinic.model.Owner;
import com.learning.petclinic.model.PetType;
//import org.junit.FixMethodOrder;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.junit.runners.MethodSorters;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNotNull;
//import static org.junit.Assert.assertTrue;

//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//@RunWith(SpringRunner.class)
////@SpringBootTest
@DataJpaTest
//@SpringBootTest
class OwnerRepositoryIT {

    @Autowired
    OwnerRepository ownerRepository;

    @Test
    void test1_save() {

        //given
        Owner owner = Owner.builder()
                .address("gagarina")
                .city("Minsk")
                .contactDetails(ContactDetails.builder()
                        .firstName("Alex")
                        .lastName("Grouk")
                        .phone("12345")
                        .build())
                .build();

        //when
        Owner result = ownerRepository.save(owner);

        //then
        assertNotNull(result);
        assertNotNull(result.getId());
//        assertNotNull(result.getCreatedAt());
        assertEquals("Minsk", result.getCity());
    }

    @Transactional
    @Test
    void test2_save2() {

        //given
        Owner owner = Owner.builder()
                .address("gagarina")
                .city("Minsk")
                .contactDetails(ContactDetails.builder()
                        .firstName("Alex")
                        .lastName("Grouk")
                        .phone("12345")
                        .build())
                .build();

        Dog dog = Dog.builder()
                .birthDate(LocalDate.now())
                .name("Rex")
                .petType(PetType.DOG)
                .owner(owner)
                .build();

        owner.setPets(Set.of(dog));

        //when
        Owner result = ownerRepository.save(owner);

        //then
        assertNotNull(result);
        assertNotNull(result.getId());

        assertEquals("Minsk", result.getCity());
        assertNotNull(result.getPets());
        assertEquals(1, result.getPets().size());
    }

    @Test
    void test3_findAll() {
        List<Owner> result = ownerRepository.findAll();

        assertNotNull(result);
//        assertEquals(3, result.size());
    }
}