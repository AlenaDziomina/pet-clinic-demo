package com.learning.petclinic.controller;

import com.learning.petclinic.model.Owner;
import com.learning.petclinic.service.OwnerService;
//import org.junit.Test;
//import org.junit.runner.RunWith;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;

import java.util.Set;

//import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

//@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class IndexControllerTest {

    @InjectMocks
    IndexController indexController;

    @Mock
    OwnerService ownerService;

    @Mock
    Model model;

    @Test
    public void index() {
        //given
        Set<Owner> ownerSet = mock(Set.class);
        when(ownerService.findAll()).thenReturn(ownerSet);

        //when
        String result = indexController.index(model);

        //then
        assertEquals("index", result);
        verify(ownerService, times(1)).findAll();
        verify(model, times(1)).addAttribute(eq("owners"), eq(ownerSet));
    }

    @Test
    public void index2() {
        //given
        Set<Owner> ownerSet = mock(Set.class);
        when(ownerService.findAll()).thenReturn(ownerSet);
        ArgumentCaptor<Set<Owner>> argumentCaptor = ArgumentCaptor.forClass(Set.class);

        //when
        String result = indexController.index(model);

        //then
        assertEquals("index", result);
        verify(ownerService, times(1)).findAll();
        verify(model, times(1)).addAttribute(eq("owners"), argumentCaptor.capture());
        assertEquals(ownerSet, argumentCaptor.getValue());
    }

    @Test
    public void index3() {
        //given
        Owner owner1 = Owner.builder().id(1L).build();
        Owner owner2 = Owner.builder().id(2L).build();
        Set<Owner> ownerSet = Set.of(owner1, owner2);
        when(ownerService.findAll()).thenReturn(ownerSet);
        ArgumentCaptor<Set<Owner>> argumentCaptor = ArgumentCaptor.forClass(Set.class);

        //when
        String result = indexController.index(model);

        //then
        assertEquals("index", result);
        verify(ownerService, times(1)).findAll();
        verify(model, times(1)).addAttribute(eq("owners"), argumentCaptor.capture());
        assertEquals(ownerSet, argumentCaptor.getValue());
        assertEquals(2, argumentCaptor.getValue().size());
    }

    @Test
    public void indexWithMockMWC() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(indexController).build();

        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }
}