package com.learning.petclinic.controller;

import com.learning.petclinic.model.Pet;
import com.learning.petclinic.service.OwnerService;
import com.learning.petclinic.service.PetService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PetControllerTest {

    @InjectMocks
    PetController petController;

    @Mock
    PetService petService;

    @Mock
    Model model;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getPets() {
        //given
        Set<Pet> petSet = mock(Set.class);
        when(petService.findAll()).thenReturn(petSet);

        //when
        String result = petController.getPets(model);

        //then
        assertEquals("pets/index", result);
        verify(petService, times(1)).findAll();
        verify(model, times(1)).addAttribute(eq("pets"),eq(petSet));
    }
}