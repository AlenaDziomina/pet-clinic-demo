package com.learning.petclinic.controller;

import com.learning.petclinic.exception.NotFoundException;
import com.learning.petclinic.model.Owner;
import com.learning.petclinic.service.OwnerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
class OwnerControllerTest {

    @InjectMocks
    OwnerController ownerController;

    @Mock
    OwnerService ownerService;

    @Mock
    Model model;
    MockMvc mockMvc;

    @BeforeEach
    void beforeEach() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(ownerController).build();
    }

    @Test
    void getOwners() {
        //given
        Set<Owner> ownerSet = mock(Set.class);
        when(ownerService.findAll()).thenReturn(ownerSet);


        //when
        String result = ownerController.getOwners(model);

        //then
        assertEquals("owners/index", result);
        verify(ownerService, times(1)).findAll();
        verify(model, times(1)).addAttribute(eq("owners"), eq(ownerSet));

    }

    @Test
    void findOwners() {
    }

    @Test
    void processFindOwners() {
        Owner owner = mock(Owner.class);
        List<Owner> ownerList = mock(List.class);
        BindingResult bindingResult = mock(BindingResult.class);
        when(ownerService.findByFullName("Name")).thenReturn(ownerList);
        when(owner.getFullName()).thenReturn("Name");
        when(ownerList.isEmpty()).thenReturn(true);

        //when
        String result = ownerController.processFindOwners(owner, bindingResult, model);

        //then
        assertEquals("owners/findOwners", result);
        verify(ownerService, times(1)).findByFullName(owner.getFullName());
        verify(bindingResult, times(1))
                .rejectValue(eq("fullName"), eq("not found"), eq("not found"));

    }

    @Test
    void processFindOwnersElse() {
        Owner owner = mock(Owner.class);
        List<Owner> ownerList = mock(List.class);
        BindingResult bindingResult = mock(BindingResult.class);
        when(ownerService.findByFullName("Name")).thenReturn(ownerList);
        when(owner.getFullName()).thenReturn("Name");
        when(ownerList.isEmpty()).thenReturn(false);

        //when
        String result = ownerController.processFindOwners(owner, bindingResult, model);

        //then
        assertEquals("owners/ownersList", result);
        verify(ownerService, times(1)).findByFullName(owner.getFullName());
        verify(model, times(1))
                .addAttribute(eq("selections"), eq(ownerList));

    }

    @Test
    void showOwner() throws Exception {
        //given
        Owner owner = Owner.builder()
                            .id(555555555L)
                            .build();
        when(ownerService.findById(123L))//.thenThrow(Exception.class);
                .thenReturn(owner);


        //when
        mockMvc.perform(get("/owners/123"))
                .andExpect(status().isOk())
                .andExpect(view().name("owners/ownerDetails"))
                .andExpect(model().attribute("owner",hasProperty("id", is(555555555L))));
    //HttpStatus
        //then
        //verifyNoInteractions(ownerService);
        verify(ownerService, times(1)).findById(123L);


    }

    @Test
    void showOwnerThrowsEx() throws Exception {
        when(ownerService.findById(123L)).thenThrow(NotFoundException.class);
        assertThrows(NotFoundException.class, () -> ownerController.showOwner(123L));
    }

    @Test
    void showOwnerThrowsEx2() throws Exception {
        when(ownerService.findById(123L)).thenThrow(NotFoundException.class);
        mockMvc.perform(get("/owners/123"))
                .andExpect(status().isNotFound())
                .andExpect(view().name("404error"));
    }

    @Test
    void newOwner() {
    }

    @Test
    void processNewOwner() throws Exception {
        //given
        Owner savesOwner = mock(Owner.class);
        when(savesOwner.getId()).thenReturn(123L);
        when(ownerService.save(any())).thenReturn(savesOwner);

        //when
        mockMvc.perform(post("/owners/new"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/owners/123"));

        //then
        verify(ownerService,times(1)).save(any());
    }

    @Test
    void updateOwner() {
    }

    @Test
    void processUpdateOwner() {
        //given
        Owner owner = mock(Owner.class);
        BindingResult bindingResult = mock(BindingResult.class);
        when(bindingResult.hasErrors()).thenReturn(true);

        //when
        String result = ownerController.processUpdateOwner(123L, owner, bindingResult);

        //then
        assertEquals("owners/createOrUpdateOwnerForm", result);


    }

    @Test
    void processUpdateOwnerElse() {
        //given
        Owner owner = mock(Owner.class);
        Owner saveOwner = mock(Owner.class);
        BindingResult bindingResult = mock(BindingResult.class);
        when(bindingResult.hasErrors()).thenReturn(false);
        when(ownerService.save(owner)).thenReturn(saveOwner);
        when(saveOwner.getId()).thenReturn(123L);
        //when
        String result = ownerController.processUpdateOwner(123L, owner, bindingResult);

        //then
        assertEquals("redirect:/owners/123", result);
        verify(owner, times(1)).setId(eq(123L));
        verify(ownerService, times(1)).save(eq(owner));
    }
}