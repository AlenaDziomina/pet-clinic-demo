package com.learning.petclinic.model;

//import org.junit.Before;
//import org.junit.Test;
//
//import static org.junit.Assert.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OwnerTest {

    Owner owner;

    @BeforeEach
    public void setUp() throws Exception {
        owner = new Owner();
    }

    @Test
    public void getAddress() {
        //given
        String address = "Some address";

        //when
        owner.setAddress(address);

        //then
        assertEquals(address, owner.getAddress());
    }

    @Test
    public void getCity() {
        //given
        String city = "Some city";

        //when
        owner.setCity(city);

        //then
        assertEquals(city, owner.getCity());
    }
}