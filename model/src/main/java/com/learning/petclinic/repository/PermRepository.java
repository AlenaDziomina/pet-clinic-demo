package com.learning.petclinic.repository;

import com.learning.petclinic.model.Permission;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermRepository extends JpaRepository<Permission, Long> {
}
