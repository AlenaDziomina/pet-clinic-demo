package com.learning.petclinic.repository;

import com.learning.petclinic.model.Product;
import com.learning.petclinic.model.ProductPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, ProductPK> {
}
