package com.learning.petclinic.repository;

import com.learning.petclinic.model.Notes;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface NotesRepository extends JpaRepository<Notes, Long> {
}
