package com.learning.petclinic.repository;

import com.learning.petclinic.model.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.OffsetDateTime;
import java.util.Optional;

public interface PetRepository extends JpaRepository<Pet, Long> {

    Optional<Pet> findByNameLikeAndBirthDateBefore(String name, OffsetDateTime dateTime);
}
