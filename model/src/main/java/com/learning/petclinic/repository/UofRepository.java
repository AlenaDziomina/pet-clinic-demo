package com.learning.petclinic.repository;

import com.learning.petclinic.model.UnitOfMeasure;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UofRepository extends JpaRepository<UnitOfMeasure, String> {
}
