package com.learning.petclinic.repository;

import com.learning.petclinic.model.Owner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;


public interface OwnerRepository extends JpaRepository<Owner, Long> {

    Optional<Owner> findByContactDetailsLastName(String lastName);

    Optional<Owner> findByContactDetailsLastNameAndContactDetailsFirstName(@Param("firstName") String fn,
                                                                           @Param("lastName") String ln);

    default Optional<Owner> findOwner2(String fn, String ln) {
        return findByContactDetailsLastNameAndContactDetailsFirstName(fn, ln);
    }

    @Query(value = "Select o from Owner o where o.contactDetails.firstName = :firstName and o.contactDetails.lastName = :lastName")
    Optional<Owner> findOwner(@Param("firstName") String fn, @Param("lastName") String ln);

    @Modifying
    @Query("DELETE from Owner o where o.modifiedAt < :lastdate")
    void deleteInactive(@Param("lastdate") Date date);

    List<Owner> findByFullNameLikeIgnoreCase(String fullName);

    List<Owner> findByFullNameContainingIgnoreCase(String fullName);

    List<Owner> findByFullNameContainsIgnoreCase(String fullName);

    List<Owner> findByFullNameIsContainingIgnoreCase(String fullName);

    List<Owner> findByFullNameStartingWith(String fullName);

    List<Owner> findByFullNameEndingWith(String fullName);







}
