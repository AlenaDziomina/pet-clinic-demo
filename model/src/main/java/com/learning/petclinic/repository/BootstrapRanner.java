package com.learning.petclinic.repository;

import com.learning.petclinic.model.*;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Set;

@Transactional
@RequiredArgsConstructor
@Component
public class BootstrapRanner implements CommandLineRunner {

    private final VisitRepository visitRepository;
    private final OwnerRepository ownerRepository;


    @Override
    public void run(String... args) throws Exception {


        VipClient owner = VipClient.builder()
                .vipCard("1111")
                .address("Lenina str")
                .city("Minsk")
                .telephone("+375 29 123 45 67")
                .contactDetails(ContactDetails.builder()
                        .firstName("Alena")
                        .lastName("Dziomina")
                        .phone("123456")
                        .build())
                .build();

        Owner owner1 = Owner.builder()
                .address("Gagarina")
                .city("Brest")
                .telephone("+375 44 555 44 33")
                .contactDetails(ContactDetails.builder()
                        .firstName("Alena")
                        .lastName("Grouk")
                        .phone("54321")
                        .build())
                .build();

        Dog dog1 = Dog.builder()
                .birthDate(LocalDate.now().minusYears(2))
                .name("Dinga")
                .petType(PetType.DOG)
                .dogName("Dog Dinga")
                .owner(owner1)
                .build();

        Dog dog = Dog.builder()
                .birthDate(LocalDate.now().minusYears(2))
                .name("Rex")
                .petType(PetType.DOG)
                .dogName("Dog Reks")
                .owner(owner)
                .build();
        Cat cat = Cat.builder()
                .birthDate(LocalDate.now().minusYears(2))
                .name("Baks")
                .petType(PetType.CAT)
                .catName("Cat Baks")
                .owner(owner)
                .build();
        owner.setPets(Set.of(dog, cat));
        owner1.setPets(Set.of(dog1));

        ownerRepository.save(owner);
        ownerRepository.save(owner1);

        Notes notes1 = Notes.builder()
                .description("Some notes 1")
                .build();

        Notes notes2 = Notes.builder()
                .description("Some notes 2")
                .build();
        Notes notes3 = Notes.builder()
                .description("Some notes 3")
                .build();

        Notes notes4 = Notes.builder()
                .description("Some notes 4")
                .build();


        Visit visit1 = Visit.builder()
                .notes(notes1)
                .pet(dog)
                .time(OffsetDateTime.now())
                .build();
        Visit visit2 = Visit.builder()
                .notes(notes2)
                .pet(cat)
                .time(OffsetDateTime.now())
                .build();
        Visit visit3 = Visit.builder()
                .notes(notes3)
                .pet(dog)
                .time(OffsetDateTime.now())
                .build();
        Visit visit4 = Visit.builder()
                .notes(notes4)
                .pet(cat)
                .time(OffsetDateTime.now())
                .build();

        visitRepository.save(visit1);
        visitRepository.save(visit2);
        visitRepository.save(visit3);
        visitRepository.save(visit4);

    }

}
