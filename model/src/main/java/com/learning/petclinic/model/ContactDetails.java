package com.learning.petclinic.model;

import lombok.*;

import javax.persistence.Embeddable;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class ContactDetails {

    private String firstName;
    private String lastName;
    private String phone;

}
