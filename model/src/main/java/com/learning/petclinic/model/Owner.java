package com.learning.petclinic.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;


@SuperBuilder
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
public class Owner extends Person {

    private String fullName;
    private boolean state;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "owner", fetch = FetchType.EAGER)
    private Set<Pet> pets;

    @NotBlank(message = "Address should not be blank")
    private String address;
    private String city;
    private String telephone;

    @PreUpdate
    @PrePersist
    private void initFullName() {
        this.fullName = getContactDetails().getFirstName() + " " + getContactDetails().getLastName();
    }
}
