package com.learning.petclinic.model;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class ProductPK implements Serializable {

    private String code;
    private String codePart;
}
