package com.learning.petclinic.model;

import lombok.*;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Product {

    @EmbeddedId
    private ProductPK productId;
    private String name;
    private BigDecimal price;
}
