package com.learning.petclinic.model;

import lombok.*;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private OffsetDateTime time;
    @OneToOne(cascade = CascadeType.ALL)
    private Notes notes;

    @ManyToOne(cascade = CascadeType.ALL)
    private Pet pet;
}
