package com.learning.petclinic.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "PET_TYPE", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("Unknown")
@NamedQueries(value = {
        @NamedQuery(name = "Pet.findAll", query = "select p from Pet p order by p.name"),
        @NamedQuery(name = "Pet.findByName", query = "select p from Pet p where p.name = :name")
})
public class Pet extends BaseEntity {

    @Column(name = "pet_name", nullable = false, unique = true)
    private String name;

    @Column(name = "PET_TYPE", nullable = false, insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private PetType petType;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;

    @ManyToOne
    private Owner owner;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pet")
    private Set<Visit> visits = new HashSet<>();

}
