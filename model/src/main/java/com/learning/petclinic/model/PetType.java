package com.learning.petclinic.model;

public enum PetType {

    CAT("Cat"),
    DOG("Dog");

    private final String name;

    public String getName() {
        return name;
    }

    PetType(String name) {
        this.name = name;
    }

    public static class Values {
        public static final String CAT = "CAT";
        public static final String DOG = "DOG";
    }
}
