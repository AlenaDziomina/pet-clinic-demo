package com.learning.petclinic.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Item {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private BigDecimal amount;
    @ManyToOne(cascade = CascadeType.PERSIST)
//    @JoinColumn(name = "receipt_id2")
    private Receipt receipt;
    @ManyToOne
    private UnitOfMeasure uof;


}
