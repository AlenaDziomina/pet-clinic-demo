package com.learning.petclinic.model;


import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum Role {

    USER("USR"), ADMIN("ADM"), MANAGER("MGR"), MAIN_OPER("OPER"), CONTACT_PERSON("CP");

    private static final Map<String, Role> map = Arrays.stream(Role.values())
            .collect(Collectors.toMap(Role::getDescription, Function.identity()));

    private final String description;

    Role(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public static Role getByDesc(String desc) {
        return map.get(desc);
    }

    @Component
    public static class RoleConverter implements AttributeConverter<Role, String> {

        @Override
        public String convertToDatabaseColumn(Role attribute) {
            return attribute.getDescription();
        }

        @Override
        public Role convertToEntityAttribute(String dbData) {
            return map.get(dbData);
        }
    }
}
