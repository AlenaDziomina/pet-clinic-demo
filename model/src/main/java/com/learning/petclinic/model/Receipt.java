package com.learning.petclinic.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Receipt {

    @Id
    @GeneratedValue
    private Long id;
    private BigDecimal amount;
    private int discount;
    @OneToMany(mappedBy = "receipt", cascade = CascadeType.ALL)
    private List<Item> items;
    @Transient
    private BigDecimal cost;

    @PostLoad
    @PostUpdate
    private void calcCost() {
        cost = amount.divide(BigDecimal.valueOf(discount), 2, RoundingMode.HALF_UP);
    }



}
