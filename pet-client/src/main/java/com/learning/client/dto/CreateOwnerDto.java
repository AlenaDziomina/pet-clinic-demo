package com.learning.client.dto;

import lombok.Data;

@Data
public class CreateOwnerDto {

    private String firstName;

    private String lastName;

    private String address;
}
