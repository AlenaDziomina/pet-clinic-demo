package com.learning.client.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@ApiModel("Owner of the pet")
@Data
public class OwnerDto {

    @ApiModelProperty("Owner id")
    private Long id;

    private String firstName;

    private String lastName;
    private String fillName;

    private String address;
}
