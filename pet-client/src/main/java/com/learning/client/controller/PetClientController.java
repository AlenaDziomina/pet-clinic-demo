package com.learning.client.controller;


import com.learning.client.dto.OwnerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/api/external/owners")
public class PetClientController {

    @Autowired
    private RestTemplate template;

    @Value("${api-url}")
    private String apiUri;

    @GetMapping
    public List<OwnerDto> getOwners() {
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(apiUri);
        return template.getForObject(uriComponentsBuilder.toUriString(), List.class);
    }
}
